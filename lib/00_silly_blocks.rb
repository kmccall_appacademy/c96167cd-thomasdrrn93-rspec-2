def reverser(&prc)
  string = yield
  string.split.map { |word| word.reverse}.join(" ")
end

def adder(num = 1, &prc)
  value = yield
  value + num
end

def repeater (call = 1, &prc)
  call.times do
    yield
  end
end     
