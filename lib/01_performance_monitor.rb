def measure(number = 1, &prc)
  start_time = Time.now
  number.times { yield }
  (Time.now - start_time) / number
end
